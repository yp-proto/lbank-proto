.PHONY:	pbgen
pbgen:
		protoc --proto_path=./ \
    		--go_out=pkg --go-grpc_out=pkg \
    		--go-grpc_opt=paths=source_relative --go_opt=paths=source_relative \
    				header/v1/header.proto \
    	 	--experimental_allow_proto3_optional
